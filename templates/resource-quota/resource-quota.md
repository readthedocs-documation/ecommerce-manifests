## resource quotas  
https://kubernetes.io/docs/concepts/policy/resource-quotas/ 
`kubectl describe quota`    
`kubectl describe namespace digital-staging`
`kubectl describe quota --namespace=digital-staging`

| Resource Name    | Description                                                                                                               |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------- |
| limits.cpu       | Across all pods in a non-terminal state, the sum of CPU limits cannot exceed this value.                                  |
| limits.memory    | Across all pods in a non-terminal state, the sum of memory limits cannot exceed this value.                               |
| requests.cpu     | Across all pods in a non-terminal state, the sum of CPU requests cannot exceed this value.                                |
| requests.memory  | Across all pods in a non-terminal state, the sum of memory requests cannot exceed this value.                             |
| hugepages-<size> | Across all pods in a non-terminal state, the number of huge page requests of the specified size cannot exceed this value. |
| cpu              | Same as requests.cpu                                                                                                      |
| memory           | Same as requests.memory                                                                                                   |



| resource        | smalll | medium | large | extra |
| --------------- | ------ | ------ | ----- | ----- |
| requests.cpu    | 100m   | 200m   | 300m  | 400m  |
| requests.memory | 100M   | 200M   | 300M  | 400M  |
| limits.cpu      | 128m   | 256m   | 400m  | 512m  |
| limits.memory   | 128M   | 256M   | 400M  | 512M  |