## ecommerce-manifests    
### gitops workflow    
We will implement a GitOps scenario using:    
    - ArgoCD as the GitOps tool    
    - GitHub Actions as the CICD pipeline    
    - Kustomize to describe application deployments    

### kustomize    
https://github.com/kubernetes-sigs/kustomize    

kustomize 术语
在 kustomize 项目的文档中，经常会出现一些专业术语，这里总结一下常见的术语:    
- **kustomization**    
术语 kustomization 指的是 kustomization.yaml 文件，或者指的是包含 kustomization.yaml 文件的目录以及它里面引用的所有相关文件路径    
- **base**    
base 指的是一个 kustomization , 任何的 kustomization 包括 overlay (后面提到)，都可以作为另一个 kustomization 的 base (简单理解为基础目录)。base 中描述了共享的内容，如资源和常见的资源配置    
- **overlay**    
overlay 是一个 kustomization, 它修改(并因此依赖于)另外一个 kustomization. overlay 中的 kustomization指的是一些其它的 kustomization, 称为其 base. 没有 base, overlay 无法使用，并且一个 overlay 可以用作 另一个 overlay 的 base(基础)。简而言之，overlay 声明了与 base 之间的差异。通过 overlay 来维护基于 base 的不同 variants(变体)，例如开发、QA 和生产环境的不同 variants     
- **variant**    
variant 是在集群中将 overlay 应用于 base 的结果。例如开发和生产环境都修改了一些共同 base 以创建不同的 variant。这些 variant 使用相同的总体资源，并与简单的方式变化，例如 deployment 的副本数、ConfigMap使用的数据源等。简而言之，variant 是含有同一组 base 的不同 kustomization
- **resource**    
在 kustomize 的上下文中，resource 是描述 k8s API 对象的 YAML 或 JSON 文件的相对路径。即是指向一个声明了 kubernetes API 对象的 YAML 文件    
- **patch**    
修改文件的一般说明。文件路径，指向一个声明了 kubernetes API patch 的 YAML 文件    

#### make a kustomization file    
YAML resource files (deployments, services, configmaps, etc.), create a kustomization file.    
This file should declare those resources, and any customization to apply to them, e.g. add a common label.    
![kustomize-base](./images/kustomize-base.jpg)    

#### create variants using overlays    
Manage traditional variants of a configuration - like development, staging and production - using overlays that modify a common base.    
![kustomize-overlay](./images/kustomize-overlay.jpg)    


### kustomize vs helm    
kustomize 与 Helm 有一定的相似。先来看看 Helm 的定位：**Kubernetes 的包管理工具**，而 kustomize 的定位是：**Kubernetes 原生配置管理**。两者定位领域有很大不同，Helm 通过将应用抽象成 Chart 来管理, 专注于应用的操作、复杂性管理等, 而 kustomize 关注于 k8s API 对象的管理。    
    - Helm 提供应用描述文件模板(Go template)，在部署时通过字符替换方式渲染成 YAML，对应用描述文件具有侵入性。Kustomize 使用原生 k8s 对象，无需模板参数化，无需侵入应用描述文件(YAML), 通过 overlay 选择相应 patch 生成最终 YAML    
    - Helm 专注于应用的复杂性及生命周期管理(包括 install、upgrade、rollback)，kustomize 通过管理应用的描述文件来间接管理应用    
    - Helm 使用 Chart 来管理应用，Chart 相对固定、稳定，相当于静态管理，更适合对外交付使用，而 kustomize 管理的是正在变更的应用，可以 fork 一个新版本，创建新的 overlay 将应用部署在新的环境，相当于动态管理，适合于 DevOps 流程    
    - Helm 通过 Chart 方式打包并管理应用版本，kustomize 通过 overlay 方式管理应用不同的变体，通过 Git 来版本管理    
    - Helm 在v3 版本前有 Helm 和 Tiller 两组件，需要一定配置，而 kustomize 只有一个二进制，开箱即用    

总的来说，Helm 有自己一套体系来管理应用，而 kustomize 更轻量级，融入Kubernetes 的设计理念，通过原生 k8s API 对象来管理应用   


```bash
brew install kustomize
go get sigs.k8s.io/kustomize
kustomize version
```

```bash
yamllint onboarding-camunda-workflow/base/*.yaml
yamllint onboarding-camunda-workflow/overlays/staging/*.yaml
yamllint onboarding-camunda-workflow/overlays/production/*.yaml
```

```bash
kustomize build --stack-trace api-gateway/base
kustomize build --stack-trace api-gateway/overlays/staging

rock@rock-HP-Z420-Workstation:~/workspace/microservices-devops/ecommerce-manifests$ kustomize build --stack-trace api-gateway/overlays/staging | kubectl apply -f -
service/staging-api-gateway created
deployment.apps/staging-api-gateway created


kubectl get all --namespace=digital-staging
kubectl get services --namespace=digital-staging | awk '{ print $1 }' 

kubectl delete deployments staging-api-gateway --namespace=digital-staging
kubectl delete services staging-api-gateway --namespace=digital-staging

kustomize build --stack-trace api-gateway/overlays/staging | kubectl --namespace=digital-staging apply -f -
```


```bash
rock@rock-HP-Z420-Workstation:~/workspace/microservices-devops/ecommerce-manifests$ kubectl get deployment --namespace=digital-staging
NAME             READY   UP-TO-DATE   AVAILABLE   AGE
details-v1       1/1     1            1           32d
productpage-v1   1/1     1            1           32d
ratings-v1       1/1     1            1           32d
reviews-v1       1/1     1            1           32d
reviews-v2       1/1     1            1           32d
reviews-v3       1/1     1            1           32d

kubectl delete deployments --namespace=digital-staging production-api-gateway

kubectl delete deployments details-v1 --namespace=digital-staging
kubectl delete deployments productpage-v1 --namespace=digital-staging
kubectl delete deployments reviews-v1 --namespace=digital-staging
kubectl delete deployments reviews-v2 --namespace=digital-staging
kubectl delete deployments reviews-v3 --namespace=digital-staging
```


```bash

```


```bash
# docker pull
rock@rock-HP-Z420-Workstation:~/workspace/microservices-devops/ecommerce$ sudo docker pull registry.gitlab.com/microservices-devops/ecommerce/api-gateway:latest
[sudo] password for rock: 
latest: Pulling from microservices-devops/ecommerce/api-gateway
540db60ca938: Pull complete 
93db73eed96e: Pull complete 
9affb1e38818: Pull complete 
891b2a08147b: Pull complete 
Digest: sha256:2ee18f82e94c754c2e4c52bcd8fd693419219d9aa2753d7ffe607d9309f14a23
Status: Downloaded newer image for registry.gitlab.com/microservices-devops/ecommerce/api-gateway:latest
registry.gitlab.com/microservices-devops/ecommerce/api-gateway:latest

# docker image ls
rock@rock-HP-Z420-Workstation:~/workspace/microservices-devops/ecommerce$ sudo docker image ls | grep 'api-gateway'
registry.gitlab.com/microservices-devops/ecommerce/api-gateway                 latest           f0f865caae93   3 hours ago     219MB
registry.gitlab.com/microservices-devops/ecommerce/api-gateway                 <none>           6f3107b23975   23 hours ago    175MB

# docker rmi 
rock@rock-HP-Z420-Workstation:~/workspace/microservices-devops/ecommerce$ sudo docker rmi 6f3107b23975
Untagged: registry.gitlab.com/microservices-devops/ecommerce/api-gateway@sha256:8803d042955e3223a467a20f5b8fb11dd3755291b3502449180150408c816930
Deleted: sha256:6f3107b23975d0e6de6228791c10a45de166f0b58205eb665708cd16c87da7d3
Deleted: sha256:2db40d1dc6e9cdbef802dd654a63f0f2df8f1024f1db7a990ed11330a95f91e4
Deleted: sha256:ebf07a0e8ca1ab42dbcfeae1d01056527f71ae10010ce8cd64c5ae5a927630d3

# docker run
rock@rock-HP-Z420-Workstation:~/workspace/microservices-devops/ecommerce$ sudo docker run registry.gitlab.com/microservices-devops/ecommerce/api-gateway:latest
07:32:45.637  [main] INFO  c.e.a.gateway.ApiGatewayApplication logStarting 55 - Starting ApiGatewayApplication v1.0-SNAPSHOT using Java 11.0.11 on 48afc385edf0 with PID 1 (/app.jar started by root in /)
07:32:45.641  [main] DEBUG c.e.a.gateway.ApiGatewayApplication logStarting 56 - Running with Spring Boot v2.4.4, Spring v5.3.5
07:32:45.641  [main] INFO  c.e.a.gateway.ApiGatewayApplication logStartupProfileInfo 662 - No active profile set, falling back to default profiles: default
...
07:32:59.702  [main] INFO  o.s.c.n.e.s.EurekaAutoServiceRegistration onApplicationEvent 144 - Updating port to 8020
07:32:59.787  [main] INFO  c.e.a.gateway.ApiGatewayApplication logStarted 61 - Started ApiGatewayApplication in 15.586 seconds (JVM running for 16.661)

# docker ps
rock@rock-HP-Z420-Workstation:~/workspace/microservices-devops/ecommerce$ sudo docker ps | grep 'api-gateway'

```