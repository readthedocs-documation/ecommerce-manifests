const { format } = require("date-fns")

module.exports = function (plop) {
  plop.setHelper('environment-overlays', function (env) {
    var inherit = 'base';
    if (env != 'base') {
      inherit = 'overlays/' + env
    }
    return inherit;
  });

  plop.setHelper('overlays', function (env) {
    var suffix = '-overlays';
    if (env == 'base') {
      suffix = '';
    }
    return suffix;
  });

  plop.setHelper('nowadays', function () {
    const date = format(Date.now(), "yyyy-MM-dd")
    return date
  });

  plop.setActionType('console', function (answers, config, plop) {
    console.log(config.parameters);
  });

  plop.setGenerator('kustomization', {
    description: 'kustomization',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/kustomization.yaml',
        templateFile: 'templates/kustomization{{overlays environment}}.yaml'
      }
    ]
  });

  plop.setGenerator('deployment', {
    description: 'deployment',
    prompts: [
      {
        type: 'input',
        name: 'groupId',
        message: 'groupId please'
      },
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'version',
        message: 'version please'
      },
      {
        type: 'input',
        name: 'branch',
        message: 'branch please'
      },
      {
        type: 'input',
        name: 'servicePort',
        message: 'servicePort please'
      },
      ,
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      },
      {
        type: 'input',
        name: 'minReplicas',
        message: 'minReplicas please'
      },
      {
        type: 'input',
        name: 'maxReplicas',
        message: 'maxReplicas please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/deployment.yaml',
        templateFile: 'templates/deployment.yaml'
      }
    ]
  });
  plop.setGenerator('ingress', {
    description: 'ingress',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'servicePort',
        message: 'servicePort please'
      },
      {
        type: 'input',
        name: 'namespace',
        message: 'namespace please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/ingress.yaml',
        templateFile: 'templates/ingress.yaml'
      }
    ]
  });
  plop.setGenerator('service', {
    description: 'service',
    prompts: [
      {
        type: 'input',
        name: 'groupId',
        message: 'groupId please'
      },
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'version',
        message: 'version please'
      },
      {
        type: 'input',
        name: 'servicePort',
        message: 'servicePort please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
    ],
    actions: [
      {
        type: 'console',
        parameters: '$groupId $artifactId $version $servicePort $environment',
      },
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/service.yaml',
        templateFile: 'templates/service.yaml'
      }
    ]
  });

  plop.setGenerator('env', {
    description: 'environment',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      },
      {
        type: 'input',
        name: 'namespace',
        message: 'namespace please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/{{environment}}.env',
        templateFile: 'templates/.env'
      }
    ]
  });
  plop.setGenerator('application', {
    description: 'application',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/application.yaml',
        templateFile: 'templates/application.yaml'
      }
    ]
  });
  plop.setGenerator('resource-quota', {
    description: 'resource-quota',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      }
      ,
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
      ,
      {
        type: 'input',
        name: 'quota',
        message: 'quota please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/resource-quota.yaml',
        templateFile: 'templates/resource-quota/resource-quota-{{quota}}.yaml'
      }
    ]
  });
  plop.setGenerator('istio-gateway', {
    description: 'istio-gateway',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/gateway.yaml',
        templateFile: 'templates/gateway.yaml'
      }
    ]
  });
  plop.setGenerator('istio-virtual-service', {
    description: 'istio-virtual-service',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/virtual-service.yaml',
        templateFile: 'templates/virtual-service.yaml'
      }
    ]
  });

  plop.setGenerator('istio-destination-rule', {
    description: 'istio-destination-rule',
    prompts: [
      {
        type: 'input',
        name: 'artifactId',
        message: 'artifactId please'
      },
      {
        type: 'input',
        name: 'environment',
        message: 'environment please'
      }
    ],
    actions: [
      {
        type: 'add',
        force: true,
        path: '{{artifactId}}/{{environment-overlays environment}}/destination-rule.yaml',
        templateFile: 'templates/destination-rule.yaml'
      }
    ]
  });
};
