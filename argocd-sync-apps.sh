#!/bin/bash
argocd app sync cart-service
argocd app sync product-catalog-service
argocd app sync currency-service
argocd app sync payment-service
argocd app sync shipping-service
argocd app sync email-service
argocd app sync checkout-service
argocd app sync recommendation-service
