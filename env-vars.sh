#!/bin/bash

# cart-service
# product-catalog-service
# currency-service
# payment-service
# shipping-service
# email-service
# checkout-service
# recommendation-service
# ad-service 

# service-discovery
# api-gateway

artifactIds=(cart-service product-catalog-service currency-service payment-service shipping-service email-service checkout-service recommendation-service ad-service service-discovery api-gateway)

function scanning() {
    touch env_settings.md
    rm env_settings.md
    touch env_settings.md

    for artifactId in "${artifactIds[@]}"
    do
        echo $artifactId/overlays/staging/staging.env >> env_settings.md
        echo -e '```bash' >> env_settings.md
        cat $artifactId/overlays/staging/staging.env  >> env_settings.md
        echo '' >> env_settings.md
        echo -e '```' >> env_settings.md
        

        echo $artifactId/overlays/production/staging.env >> env_settings.md
        echo -e '```bash' >> env_settings.md
        cat $artifactId/overlays/production/production.env  >> env_settings.md
        echo '' >> env_settings.md
        echo -e '```' >> env_settings.md
    done
}

function add_env_variable() {    
    echo "$# parameters"
    echo "parameters: $@"

    local environment=$2
    local key=$3
    local value=$4

    for artifactId in "${artifactIds[@]}"
    do
        if [ "$environment" = "staging" ]; then
            echo 'amend' $key=$value into $artifactId/overlays/staging/staging.env
        fi

        if [ "$environment" = "production" ]; then
            echo 'amend' $key=$value into $artifactId/overlays/production/production.env
        fi
    done
}

case "$1" in
        scanning)
                scanning
                ;;
        add_env_variable)
                add_env_variable
                ;;
        *)
                echo "Usage: $0 {scanning|add_env_variable}"
                echo ""
                echo "Use this shell script to environment settings."
esac