#!/bin/bash

export namespace=$1

echo "#!/bin/bash" > argocd-create-apps.sh
echo "#!/bin/bash" > argocd-sync-apps.sh
echo "#!/bin/bash" > yamllint.sh
echo "#!/bin/bash" >   kubernetes-lint.sh

sed 1d artifacts-$namespace.csv | while IFS=, read -r artifactId groupId branch version servicePort quota minReplicas maxReplicas
do
    # echo "$artifactId $groupId $branch $servicePort $quota $minReplicas $maxReplicas"
    sed 1d environments.csv | while IFS=, read -r environment ns
    do
        # echo "$namespace $artifactId $groupId $branch $servicePort $quota $minReplicas $maxReplicas $environment"
        touch $artifactId/README.md
        echo '## '$artifactId'  ' > $artifactId/README.md
        mkdir -p $artifactId/overlays/$environment

        npm run plop kustomization --artifactId $artifactId --environment $environment
        npm run plop env --artifactId $artifactId --environment $environment --namespace $namespace
        npm run plop application --artifactId $artifactId --environment $environment
        npm run plop resource-quota --artifactId $artifactId --environment $environment --quota $quota

        npm run plop deployment --artifactId $artifactId --groupId $groupId --version $version --branch $branch --servicePort $servicePort --minReplicas $minReplicas --maxReplicas $maxReplicas --environment $environment

        npm run plop ingress --artifactId $artifactId --servicePort $servicePort --namespace $namespace --environment $environment
        npm run plop service --artifactId $artifactId --groupId $groupId --version $version --servicePort $servicePort --environment $environment

        npm run plop istio-gateway --artifactId $artifactId --environment $environment
        npm run plop istio-virtual-service --artifactId $artifactId --environment $environment
        npm run plop istio-destination-rule --artifactId $artifactId --environment $environment        
    done

    echo "argocd app create ${artifactId} --repo https://gitlab.com/readthedocs-documation/ecommerce-manifests.git --revision master --path ${artifactId}/overlays/staging --dest-namespace digital-staging --dest-server https://kubernetes.default.svc --kustomize-image registry.gitlab.com/microservices-devops/ecommerce/${artifactId}:latest --loglevel debug" >> argocd-create-apps.sh
    echo "argocd app sync ${artifactId}" >> argocd-sync-apps.sh

    echo 'kubeval' $artifactId'/base/*.yaml' >>   kubernetes-lint.sh
    echo 'kubeval' $artifactId'/overlays/staging/*.yaml' >>   kubernetes-lint.sh
    echo 'kubeval' $artifactId'/overlays/production/*.yaml' >>   kubernetes-lint.sh
done
